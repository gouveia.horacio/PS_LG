Prueba de Código
================

Ejercicio 1
-----------

Dada una lista de números, determinar para cada uno si es perfecto, abundante o defectivo. La explicación de cada uno está en la [Wikipedia](https://es.wikipedia.org/wiki/N%C3%BAmero_perfecto)

Para hacer la comprobación se debe llamar a la función __determines__ pasándole la lista de los números naturales.

Para la implementación del algoritmo se realizó dos funciones distintas (__sum_divisible_1__ y __sum_divisible_2__) con el fin de medir los tiempos de ejecución. Los tiempos de ejecución de __sum_divisible_2__ fueron menores, y se ha dejado esa por defecto.


Ejercicio 2
-----------

El desarrollo de este problema se hizo a modo de plugin. Es decir, se importa el fichero __js__ en el HTML y se llama a la función que inicializa los datos para los gráficos.

Se ha desarrollado en usando javascript y el plugin de __Highcharts__.

Para llamarlo, habría que pasarle una estructura con las urls a consultar y el tipo de datos que se va a obtener. Los dos siguientes parámetros son dos funciones que reciben los datos para las gráficas. En este caso específico, las estructuras sólo valdría para el plugin de __Highcharts__.

La manera en la que fue pensado la resolución del problema fue en crear una estructura que por cada categoría tenga un objeto con la fecha en formato numérico como clave y el valor del dato como valor de la clave. De está manera nos ahorramos el tener que ordenar por fechas. Siempre que se agregue una fecha para una categoría, se debió insertar en todas las categorías para evitar errores en el gráfico.
