lt = {
  arrDate:[],
  arrCats:{},
  chart1: null,
  chart2: null,
  init: function(params, fun1, fun2) {
    if (!params || typeof params !== 'object' || !params.data) {
      lt.showError('params-mandatory');
      return false;
    }

    if (typeof params.data !== 'object') {
      lt.showError('data-must-be-array');
      return false;
    }

    if (!fun1 || !fun2) {
      lt.showError('functions-mandatory');
      return false;
    }

    lt.chart1 = fun1;
    lt.chart2 = fun2;

    for (var g in params.data) {

      if (!params.data[g].type) {
        lt.showError('type-mandatory');
        return false;
      }

      var data = lt.request(params.data[g].url);

      if (!data) {
        lt.showError('error-request');
        return false;
      }

      lt.insertData(data, params.data[g].type);
    }

    var x_axis = lt.getXAxis1();
    var series = lt.getSeries1();
    var series2 = lt.getSeries2();
    lt.chart1(x_axis, series);
    lt.chart2(series2);
  },
  // return formated date
  getFormatDate(date) {
    var d = new Date(date);
    return [d.getFullYear(), ((d.getMonth() + 1) > 9 ? '' : '0') + (d.getMonth() + 1), (d.getDate() > 9 ? '' : '0') + d.getDate()].join('');
  },
  // insert value data into categories
  insertData: function(data, type) {
    for (var i = 0; i < data.length; i++) {
      var resp = lt.getParsedDate(data[i], type);
      var date = resp[0];
      var cat = resp[1];
      var value = resp[2];

      // reviso en fechas y si no la inserto
      if (lt.arrDate.indexOf(date) < 0) {
        lt.arrDate.push(date);
      }

      // reviso si existe la categoria y si no la creo
      if (!lt.arrCats[cat]) {
        lt.arrCats[cat] = {};
        lt.arrDate.map(function(k) {
          lt.arrCats[cat][k] = 0;
        });
      }

      // inserto o sumo
      var cats = Object.keys(lt.arrCats);
      for (var j = 0; j < cats.length; j++) {

        if (!lt.arrCats[cats[j]][date])
          lt.arrCats[cats[j]][date] = 0;

        if (cat == cats[j])
          lt.arrCats[cats[j]][date] =  lt.arrCats[cats[j]][date] + value;
      }

    }
  },
  // return parsed item data according to the type of url
  getParsedDate: function(data, type) {
    switch (type) {
      case 1:
        return [lt.getFormatDate(data.d), data.cat.toUpperCase(), data.value];
        break;
      case 2:
        return [lt.getFormatDate(data.myDate), data.categ.toUpperCase(), data.val];
        break;
      case 3:
        var match_date = data.raw.match(/(\d{4}-\d{2}-\d{2})/g);
        var match_cat = data.raw.match(/#(.*)#/g);
        return [lt.getFormatDate(match_date[0]), match_cat[0].replace(/#/g, "", "gi").toUpperCase(), data.val];
        break;
      default:
        lt.showError('unknow-type');
        return false;
    }
  },
  // show alert error
  showError: function(code) {
    alert('Ha ocurrido un error ' + code);
  },
  // return formated date to line chart
  getXAxis1: function() {
    var months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun","Jul", "Ago", "Sep", "Oct", "Nov", "Dec"];
    var d = {};
    lt.arrDate.map(function(elem) {
      d[parseInt(elem)] = parseInt(elem);
    });
    return Object.keys(d).map(function(elem) {
      var elem_str = elem.toString();
      var date = new Date(elem_str.substring(0,4)+'-'+elem_str.substring(4,6)+'-'+elem_str.substring(6));
      return date.getDate() + ' '+months[date.getMonth()]+ ' '+date.getFullYear().toString().substr(-2);
    })
  },
  // return formated data to line chart
  getSeries1: function() {
    var cats = Object.keys(lt.arrCats);
    return cats.map(function(e) {
      return {
        name: e,
        data: Object.values(lt.arrCats[e])
      }
    });
  },
  // return formated data to pie chart
  getSeries2: function() {
    var cats = Object.keys(lt.arrCats);
    return cats.map(function(e) {
      return {
        name: e,
        y: Object.values(lt.arrCats[e]).reduce(function(f,s){
          return f + s;
        })
      }
    });
  },
  // make the request and return url
  request: function(url) {
    var xhr = new window.XMLHttpRequest();
    var responde = false;
    xhr.open('GET', url, false);
    xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
    xhr.addEventListener("load", function() {
      if (this.status != 200) {
        lt.showError('file-unloaded:'+url);
        return false;
      }
      try {
        response = JSON.parse(this.responseText);
      } catch (err) {
        response = false;
      }
    }, false);
    xhr.addEventListener("error", function(evt) {
      response = false;
    }, true);
    xhr.send();
    return response;
  },
}
