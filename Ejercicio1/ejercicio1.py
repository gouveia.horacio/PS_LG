__author__ = "J. Horacio Gouveia"

def determines(elem):
    if type(elem) is not list:
        print("El valor pasado no es una lista")
        return
    for x in elem:
        print("Comprobando con " + str(x))
        if type(x) is not int or x <= 0:
            print (str(x) + " no es un numero natural")
            continue
        sum_divisible = sum_divisible_2(x)
        if x == sum_divisible:
            print(str(x) + " es un numero perfecto")
        elif x > sum_divisible:
            print(str(x) + " es un numero abundante")
        else:
            print(str(x) + " es un numero defectivo")

def sum_divisible_1(num):
    return sum(list(filter(lambda x: num % x == 0, list(range(1,num)))))

def sum_divisible_2(num):
    x = 1
    suma = 0
    while x < num:
        if num % x == 0:
            suma = suma + x
        x = x + 1

    return suma

def main():
    lns = [6, 12, 16, 28, 18, 19, 1291, 9876, 8128, 0 , -1, 'a', [7]]
    determines(lns)

if __name__ == "__main__":
    main()
